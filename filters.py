from dateutil.parser import parse as parse_date

def format_datetime(value, format=r'%Y %m %d, %H:%M'):
    dt = parse_date(value)
    return dt.strftime(format)

def select_date(value, date):
    res = []
    for v in value:
        if v.meta.start.startswith(date):
            res.append(v)
    print(res)
    return res